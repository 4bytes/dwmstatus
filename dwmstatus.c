#define _GNU_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <strings.h>
#include <sys/time.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>

#include <X11/Xlib.h>
#include <alsa/asoundlib.h>

#include <curl/curl.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#define WOID "20070188" /* Kiev city */
#define UNIT "c"        /* Celsius unit */
#define URL "http://weather.yahooapis.com/forecastrss?w="WOID"&u="UNIT"&d=1"

#define XC(t) ((const xmlChar *)(t))

/* icons */
#define WTHR_ICON "\uF0E9"
#define MAIL_ICON "\uF0E0"
#define TIME_ICON "\uF017"
#define VOLUP_ICON "\uF028"
#define VOLDOWN_ICON "\uF027"
#define VOLOFF_ICON "\uF026"
#define SEPR_ICON " "

char *tzkiev = "Europe/Kiev";

static Display *dpy;

char *
smprintf(char *fmt, ...)
{
	va_list fmtargs;
	char *ret;
	int len;

	va_start(fmtargs, fmt);
	len = vsnprintf(NULL, 0, fmt, fmtargs);
	va_end(fmtargs);

	ret = malloc(++len);
	if (ret == NULL) {
		perror("malloc");
		exit(1);
	}

	va_start(fmtargs, fmt);
	vsnprintf(ret, len, fmt, fmtargs);
	va_end(fmtargs);

	return ret;
}

void
settz(char *tzname)
{
	setenv("TZ", tzname, 1);
}

char *
mktimes(char *fmt, char *tzname)
{
	char buf[129];
	time_t tim;
	struct tm *timtm;

	memset(buf, 0, sizeof(buf));
	settz(tzname);
	tim = time(NULL);
	timtm = localtime(&tim);
	if (timtm == NULL) {
		perror("localtime");
		exit(1);
	}

	if (!strftime(buf, sizeof(buf)-1, fmt, timtm)) {
		fprintf(stderr, "strftime == 0\n");
		exit(1);
	}

	return smprintf("%s", buf);
}

void
setstatus(char *str)
{
	XStoreName(dpy, DefaultRootWindow(dpy), str);
	XSync(dpy, False);
}

char *
loadavg(void)
{
	double avgs[3];

	if (getloadavg(avgs, 3) < 0) {
		perror("getloadavg");
		exit(1);
	}

	return smprintf("%.2f %.2f %.2f", avgs[0], avgs[1], avgs[2]);
}

char *get_nmail(char *directory)
{
    /* directory : Maildir path
    * return label : number_of_new_mails
    */

    int n = 0;
    DIR* dir = NULL;
    struct dirent* rf = NULL;

    dir = opendir(directory); /* try to open directory */
    if (dir == NULL)
        perror("");

    while ((rf = readdir(dir)) != NULL) /*count number of file*/
    {
        if (strcmp(rf->d_name, ".") != 0 &&
            strcmp(rf->d_name, "..") != 0)
            n++;
    }
    closedir(dir);
    return smprintf("%d", n);
}

struct MemoryStruct {
	char *memory;
	size_t size;
};

struct WeatherInfo {
	char *date;
	char *temp;
	char *cond;
};

static size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp) {
	size_t realsize = size * nmemb;
	struct MemoryStruct *mem = (struct MemoryStruct *)userp;

	mem->memory = realloc(mem->memory, mem->size + realsize + 1);
	if(mem->memory == NULL) {
		printf("not enough memory (realloc returned NULL)\n");
		return 0;
	}

	memcpy(&(mem->memory[mem->size]), contents, realsize);
	mem->size += realsize;
	mem->memory[mem->size] = 0;

	return realsize;
}

static struct WeatherInfo xmlparser(const char *xmlbuf, int length) {
	xmlDocPtr doc;
	xmlNodePtr cur;
	struct WeatherInfo data;

	doc = xmlReadMemory(xmlbuf, length, "noname.xml", NULL, 0);
	if (doc == NULL) {
		fprintf(stderr, "Not parsed\n");
		return;
	}

	cur = xmlDocGetRootElement(doc);

	if (cur == NULL) {
		fprintf(stderr, "empty doc\n");
		xmlFreeDoc(doc);
		return;
	}
	if (xmlStrcmp(cur->name, (const xmlChar *) "rss")) {
		fprintf(stderr, "Wrong root node\n");
		xmlFreeDoc(doc);
		return;
	}

	cur = cur->xmlChildrenNode;

	xmlXPathContextPtr context;
	xmlXPathObjectPtr result;

	context = xmlXPathNewContext(doc);

	xmlXPathRegisterNs(context, XC("yweather"), XC("http://xml.weather.yahoo.com/ns/rss/1.0"));
	result = xmlXPathEval(XC("/rss/channel/item/yweather:condition"), context);

	if(xmlXPathNodeSetIsEmpty(result->nodesetval)) {
		xmlXPathFreeObject(result);
		printf("Иди нахуй\n");
	}
	if (!result || result->type != XPATH_NODESET)
		printf("Дом-2\n");

	xmlNodePtr node;
	xmlChar *date_val;
	xmlChar *temp_val;
	xmlChar *text_val;

	node = result->nodesetval->nodeTab[0];
	text_val = xmlGetProp(node, XC("text"));
	date_val = xmlGetProp(node, XC("date"));
	temp_val = xmlGetProp(node, XC("temp"));

	data.date = date_val;
	data.temp = temp_val;
	data.cond = text_val;
    xmlFree(node);
	// printf("Date: %s Temp: %s: Cond: %s\n", data.date, data.temp, data.cond);
	return data;
}

char *get_weather() {
	CURL *curl_handle;
	CURLcode res;

	struct MemoryStruct chunk;
	struct WeatherInfo info;

	chunk.memory = malloc(1);  /* will be grown as needed by the realloc above */
	chunk.size = 0;    /* no data at this point */

	curl_global_init(CURL_GLOBAL_ALL);

	/* init the curl session */
	curl_handle = curl_easy_init();

	/* specify URL to get */
	curl_easy_setopt(curl_handle, CURLOPT_URL, URL);

	/* send all data to this function  */
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

	/* we pass our 'chunk' struct to the callback function */
	curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);

	/* some servers don't like requests that are made without a user-agent
	   field, so we provide one */
	curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");

	/* get it! */
	res = curl_easy_perform(curl_handle);

	/* check for errors */
	if(res != CURLE_OK) {
		fprintf(stderr, "curl_easy_perform() failed: %s\n",
			curl_easy_strerror(res));
        info.date = "n/a";
        info.temp = "n/a";
        info.cond = "n/a";
	}
	else {
		/* printf("%lu bytes retrieved\n", (long)chunk.size); */
		/* printf("+++++++++++++++++++++++++++++++++++\n"); */
		/* printf("Here is the output: %s\n", chunk.memory); */
		/* printf("+++++++++++++++++++++++++++++++++++\n"); */

		info = xmlparser(chunk.memory, (long)chunk.size);
		xmlCleanupParser();
		xmlMemoryDump();
	}

	/* printf("%s %s %s", info.date, info.temp, info.cond); */

	/* cleanup curl stuff */
	curl_easy_cleanup(curl_handle);

	if(chunk.memory)
		free(chunk.memory);

	/* we're done with libcurl, so clean it up */
	curl_global_cleanup();
    if (info.temp == NULL || info.cond == NULL)
         return smprintf("n/a");
    return smprintf("%s°C %s", info.temp, info.cond);

}

char *getbat()
{
     FILE *f;
     int bat;
     char *baticon;

     f = fopen("/sys/class/power_supply/BAT0/capacity", "r");
     fscanf(f, "%d", &bat);
     fclose(f);

     if (bat <= 20)
          baticon = "\uF244";
     if (bat > 20 && bat <= 40)
          baticon = "\uF243";
     if (bat > 40 && bat <= 60)
          baticon = "\uF242";
     if (bat > 60 && bat <= 80)
          baticon = "\uF241";
     if (bat > 80)
          baticon = "\uF240";

     return smprintf("%s %d", baticon, bat);
}

char *get_volume(char *str)
{
     snd_mixer_t *handle;
     snd_mixer_elem_t *elem;
     snd_mixer_selem_id_t *s_elem;

     char *volicon;

     snd_mixer_open(&handle, 0);
     snd_mixer_attach(handle, "default");
     snd_mixer_selem_register(handle, NULL, NULL);
     snd_mixer_load(handle);
     snd_mixer_selem_id_malloc(&s_elem);
     snd_mixer_selem_id_set_name(s_elem, "Master");

     elem = snd_mixer_find_selem(handle, s_elem);

     if (NULL == elem)
     {
          snd_mixer_selem_id_free(s_elem);
          exit(EXIT_FAILURE);
     }

         long int vol, max, min, percent;

    snd_mixer_selem_get_playback_volume_range(elem, &min, &max);
    snd_mixer_selem_get_playback_volume(elem, 0, &vol);

    percent = (vol * 100) / max;

    snd_mixer_selem_id_free(s_elem);
    snd_mixer_close(handle);

    if (percent == 0)
         volicon = VOLOFF_ICON;
    if (percent <= 50 && percent != 0)
         volicon = VOLDOWN_ICON;
    if (percent > 50)
         volicon = VOLUP_ICON;

    return smprintf("%s %ld", volicon, percent);
}

int
main(void)
{
	char *nmail;
	char *status;
	char *tmkv;
	char *wthr;
    char *battery;
    char *volume;
    char vol[200];

    int step = 1;
    int wticks = 0;

	if (!(dpy = XOpenDisplay(NULL))) {
		fprintf(stderr, "dwmstatus: cannot open display.\n");
		return 1;
	}

    /* first we need to fetch data befor infinite loop */
    wthr = get_weather();

	for (;;sleep(step)) {
        /* we do not want to fetcha data every second */
        wticks += step;
        if (wticks % 3600 == 0) {
             wthr = get_weather();
             wticks = 0;
        }

		nmail = get_nmail("/home/int/Mail/intelegator/INBOX/new");
		tmkv = mktimes("%a %d %b %H:%M:%S", tzkiev);
        battery = getbat();
        volume = get_volume(vol);

		status = smprintf("%s %s %s %s %s %s %s %s %s %s %s %s",
                          volume, SEPR_ICON,
                          battery, SEPR_ICON,
                          WTHR_ICON, wthr, SEPR_ICON,
                          MAIL_ICON, nmail, SEPR_ICON,
                          TIME_ICON, tmkv);
		setstatus(status);
		free(tmkv);
		free(status);

	}

	XCloseDisplay(dpy);

	return 0;
}
