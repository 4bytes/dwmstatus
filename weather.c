#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#define WOID "20070188" /* Kiev city */
#define UNIT "c"        /* Celsius unit */
#define URL "http://weather.yahooapis.com/forecastrss?w="WOID"&u="UNIT"&d=1"

#define XC(t) ((const xmlChar *)(t))

char wtemp;
char wcond;

struct MemoryStruct {
	char *memory;
	size_t size;
};

static size_t WriteMemoryCallback(void *contents, size_t size, 
                                  size_t nmemb, void *userp) {
	size_t realsize = size * nmemb;
	struct MemoryStruct *mem = (struct MemoryStruct *)userp;

	mem->memory = realloc(mem->memory, mem->size + realsize + 1);
	if(mem->memory == NULL) {
		printf("not enough memory (realloc returned NULL)\n");
		return 0;
	}

	memcpy(&(mem->memory[mem->size]), contents, realsize);
	mem->size += realsize;
	mem->memory[mem->size] = 0;

	return realsize;
}

static void xmlparser(const char *xmlbuf, int length) {
	xmlDocPtr doc;
	xmlNodePtr cur;
    extern char wtemp;
    extern char wcond;

	doc = xmlReadMemory(xmlbuf, length, "noname.xml", NULL, 0);
	if (doc == NULL) {
		fprintf(stderr, "Not parsed\n");
		return;
	}

	cur = xmlDocGetRootElement(doc);

	if (cur == NULL) {
		fprintf(stderr, "empty doc\n");
		xmlFreeDoc(doc);
		return;
	}
	if (xmlStrcmp(cur->name, (const xmlChar *) "rss")) {
		fprintf(stderr, "Wrong root node\n");
		xmlFreeDoc(doc);
		return;
	}

	cur = cur->xmlChildrenNode;

	xmlXPathContextPtr context;
	xmlXPathObjectPtr result;

	context = xmlXPathNewContext(doc);

	xmlXPathRegisterNs(context, XC("yweather"), 
                       XC("http://xml.weather.yahoo.com/ns/rss/1.0"));
	result = xmlXPathEval(XC("/rss/channel/item/yweather:condition"), context);

	if(xmlXPathNodeSetIsEmpty(result->nodesetval)) {
		xmlXPathFreeObject(result);
		printf("Иди нахуй\n");
	}
	if (!result || result->type != XPATH_NODESET)
		printf("Дом-2\n");

	xmlNodePtr node;
	xmlChar *temp;
	xmlChar *cond;

	node = result->nodesetval->nodeTab[0];
	temp = xmlGetProp(node, XC("temp"));
    cond = xmlGetProp(node, XC("text"));
    printf("%s %s", (char *)temp, (char *)cond);
    wtemp = (char)temp;
    wcond = (char)cond;
}

int main() {
	CURL *curl_handle;
	CURLcode res;

    extern char wcond;
    extern char wtemp;

	struct MemoryStruct chunk;

    /* will be grown as needed by the realloc above */
	chunk.memory = malloc(1);
	chunk.size = 0;

	curl_global_init(CURL_GLOBAL_ALL);

	/* init the curl session */
	curl_handle = curl_easy_init();

	/* specify URL to get */
	curl_easy_setopt(curl_handle, CURLOPT_URL, URL);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

	/* we pass our 'chunk' struct to the callback function */
	curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);
	curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");

	res = curl_easy_perform(curl_handle);

	if(res != CURLE_OK) {
		fprintf(stderr, "curl_easy_perform() failed: %s\n",
			curl_easy_strerror(res));
	}
	else {
		xmlparser(chunk.memory, (long)chunk.size);
		xmlCleanupParser();
		xmlMemoryDump();
	}

	/* cleanup curl stuff */
	curl_easy_cleanup(curl_handle);

	if(chunk.memory)
		free(chunk.memory);

	/* we're done with libcurl, so clean it up */
	curl_global_cleanup();

    printf("%s %s", wtemp, wcond);

	return 0;
}
